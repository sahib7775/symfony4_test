<?php

namespace App\Repository;

use App\Entity\UserdRegime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserdRegime|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserdRegime|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserdRegime[]    findAll()
 * @method UserdRegime[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserdRegimeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserdRegime::class);
    }

//    /**
//     * @return UserdRegime[] Returns an array of UserdRegime objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserdRegime
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
