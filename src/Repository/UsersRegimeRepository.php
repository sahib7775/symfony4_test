<?php

namespace App\Repository;

use App\Entity\UsersRegime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UsersRegime|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsersRegime|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsersRegime[]    findAll()
 * @method UsersRegime[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersRegimeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UsersRegime::class);
    }

    public function checkRow($user_id, $regime_id){
        return (boolean) $this->createQueryBuilder('u')
            ->andWhere('u.user_id = '.$user_id.' AND u.regime_id = '.$regime_id)
            ->getQuery();
    }
}
