<?php

namespace App\Repository;

use App\Entity\RegimeDetails;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RegimeDetails|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegimeDetails|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegimeDetails[]    findAll()
 * @method RegimeDetails[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegimeDetailsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RegimeDetails::class);
    }

//    /**
//     * @return RegimeDetails[] Returns an array of RegimeDetails objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RegimeDetails
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
