<?php

namespace App\Repository;

use App\Entity\UsersRegime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\User;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getUsersRegime(){
        return $this->createQueryBuilder('u')
            ->select('u.id,u.email, u.username ,u.roles , ur.user_id, ur.regime_id, r.name as regime_name')
            ->leftJoin('u.userRegime', 'ur')
            ->leftJoin('ur.regime', 'r')
            ->getQuery()
            ->execute();
/*
        var_dump($query->getSQL());

        var_dump($query->getParameters()); die;*/
    }

}
