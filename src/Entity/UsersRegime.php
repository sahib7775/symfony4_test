<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersRegimeRepository")
 */
class UsersRegime
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $user_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $regime_id;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="usersRegime", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="Regime", mappedBy="usersRegime", cascade={"persist"})
     * @ORM\JoinColumn(name="regime_id", referencedColumnName="id")
     */
    private $regime;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId($user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getRegimeId(): ?int
    {
        return $this->regime_id;
    }

    public function setRegimeId($regime_id): self
    {
        $this->regime_id = $regime_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getRegime()
    {
        return $this->regime;
    }

    /**
     * @param mixed $regime
     */
    public function setRegime($regime)
    {
        $this->regime = $regime;
    }
}
