<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RegimeDetailsRepository")
 */
class RegimeDetails
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="integer")
     */
    private $regime_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $weekday;

    /**
     * @ORM\Column(type="string")
     */
    private $_from;

    /**
     * @ORM\Column(type="string")
     */
    private $_to;

    /**
     * @ORM\Column(type="string")
     */
    private $break_from;

    /**
     * @ORM\Column(type="string")
     */
    private $break_to;

    /**
     * @ORM\Column(type="integer")
     */
    private $active;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWeekday(): ?int
    {
        return $this->weekday;
    }

    public function setWeekday(int $weekday): self
    {
        $this->weekday = $weekday;

        return $this;
    }

    public function getFrom()
    {
        return $this->_from;
    }

    public function setFrom($_from): self
    {
        $this->_from = $_from;

        return $this;
    }

    public function getTo()
    {
        return $this->_to;
    }

    public function setTo($_to): self
    {
        $this->_to = $_to;

        return $this;
    }

    public function getBreakFrom()
    {
        return $this->break_from;
    }

    public function setBreakFrom($break_from): self
    {
        $this->break_from = $break_from;

        return $this;
    }

    public function getBreakTo()
    {
        return $this->break_to;
    }

    public function setBreakTo($break_to): self
    {
        $this->break_to = $break_to;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegimeId()
    {
        return $this->regime_id;
    }

    /**
     * @param mixed $regime_id
     */
    public function setRegimeId($regime_id)
    {
        $this->regime_id = $regime_id;
    }
}
