<?php

namespace App\Controller\Admin;

use App\Entity\Regime;
use App\Entity\RegimeDetails;
use App\Entity\User;
use App\Entity\UsersRegime;
use App\Repository\RegimeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * @Route("/admin/regime")
 */
class RegimeController extends AbstractController
{
    /**
     * @Route("/", name="regime_index", methods="GET")
     */
    public function index(RegimeRepository $regimeRepository): Response
    {
        $weekdays = [
            1   =>'Bazar ertəsi',
            2   =>'Çərşənbə axşamı',
            3   =>'Çərşənbə',
            4   =>'Cümə axşamı',
            5   =>'Cümə',
            6   =>'Şənbə',
            7   =>'Bazar'
        ];
        $response = [
            'regimes'   => $regimeRepository->findAll(),
            'weekdays'  => $weekdays
        ];
        return $this->render('regime/index.html.twig', $response);
    }

    /**
     * @Route("/new", name="regime_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $regime = new Regime();

        $em = $this->getDoctrine()->getManager();


        $from = $request->get('_from');
        $to = $request->get('_to');
        $break_from = $request->get('break_from');
        $break_to = $request->get('break_to');
        $active = $request->get('active', false);

        $regime->setName($request->get('name'));
        $em->persist($regime);
        $em->flush();

        foreach ($from as $key=>$value){
            if(!isset($active[$key])) continue;
            $RegimeDetails = new RegimeDetails();
            $RegimeDetails->setRegimeId($regime->getId());
            $RegimeDetails->setWeekday($key);
            $RegimeDetails->setFrom($value);
            $RegimeDetails->setTo($to[$key]);
            $RegimeDetails->setBreakFrom($break_from[$key]);
            $RegimeDetails->setBreakTo($break_to[$key]);
            $RegimeDetails->setActive(1);
            $em->persist($RegimeDetails);
            $em->flush();
        }



        return $this->redirectToRoute('regime_index');

    }

    /**
     * @Route("/{id}", name="regime_show", methods="GET")
     */
    public function show(Regime $regime): Response
    {
        return $this->render('regime/show.html.twig', ['regime' => $regime]);
    }

    /**
     * @Route("/{id}/edit", name="regime_edit", methods="GET|POST")
     */
    public function edit(Request $request, Regime $regime): Response
    {

        $data = json_decode($request->getContent(), true);
        $request->request->replace($data);

        $name = $request->get('name');

        $em = $this->getDoctrine()->getManager();
        $regime->setName($name);
        $em->persist($regime);
        $em->flush();
        $details = $request->get('details');

        foreach ($details as $item){

            if(!is_array($item)) continue;

            isset($item['from']) OR $item['from'] = '';
            isset($item['to']) OR $item['to'] = '';
            isset($item['breakFrom']) OR $item['breakFrom'] = '';
            isset($item['breakTo']) OR $item['breakTo'] = '';


            if( isset($item['id'])){
                $RegimeDetails = $this->getDoctrine()
                    ->getRepository(RegimeDetails::class)
                    ->find($item['id']);

                $RegimeDetails->setWeekday($item['weekday']);
                $RegimeDetails->setFrom($item['from']);
                $RegimeDetails->setTo($item['to']);
                $RegimeDetails->setBreakFrom($item['breakFrom']);
                $RegimeDetails->setBreakTo($item['breakTo']);
                $RegimeDetails->setActive(($item['active'])?1:0);
                $em->persist($RegimeDetails);
                $em->flush();
                continue;

            }

            $RegimeDetails = new RegimeDetails();
            $RegimeDetails->setRegimeId($regime->getId());
            $RegimeDetails->setWeekday($item['weekday']);
            $RegimeDetails->setFrom($item['from']);
            $RegimeDetails->setTo($item['to']);
            $RegimeDetails->setBreakFrom($item['breakFrom']);
            $RegimeDetails->setBreakTo($item['breakTo']);
            $RegimeDetails->setActive(($item['active'])?1:0);
            $em->persist($RegimeDetails);
            $em->flush();
        }

        return $this->json(['success'=>true]);

    }

    /**
     * @Route("/{id}", name="regime_delete", methods="DELETE")
     */
    public function delete(Request $request, Regime $regime): Response
    {
        if ($this->isCsrfTokenValid('delete'.$regime->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($regime);
            $em->flush();
        }

        return $this->redirectToRoute('regime_index');
    }

    /**
     * @Route("/details/{id}", name="regime_details", methods="GET")
     */
    public function details(Request $request, Regime $regime): Response
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $repository = $this->getDoctrine()->getRepository(RegimeDetails::class);
        $data = $repository->findBy(['regime_id' => $regime->getId()]);

        return new Response($serializer->serialize($data, 'json'));
    }



    /**
     * @Route("/all/show", name="regime_all", methods="GET")
     */
    public function allData(Request $request) :Response
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $repository = $this->getDoctrine()->getRepository(Regime::class);
        $data = $repository->findAll();

        return new Response($serializer->serialize($data, 'json'));
    }

    /**
     * @Route("/changeUserRegime", name="regime_change", methods="POST")
     */
    public function changeUserRegime(Request $request) :Response
    {

        $data = json_decode($request->getContent(), true);
        $request->request->replace($data);

        $user_id = (int) $request->get('user_id');
        $regime_id = (int) $request->get('regime_id');




        $entityManager = $this->getDoctrine()->getManager();
        $UsersRegime = $entityManager
            ->getRepository(UsersRegime::class)
            ->findOneBy(['user_id'=>$user_id]);

        if($UsersRegime === null) $UsersRegime = new UsersRegime();


        $UsersRegime->setUser($entityManager->getRepository(User::class)->find($user_id));
        $UsersRegime->setRegime($entityManager->getRepository(Regime::class)->find($regime_id));
        $UsersRegime->setRegimeId($regime_id);
        $entityManager->persist($UsersRegime);
        $entityManager->flush();

        return $this->json([]);
    }



}
