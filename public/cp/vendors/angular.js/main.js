var App = angular.module('App', ['ngRoute'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

App.config(function($routeProvider) {

});

App.filter('offset', function() {
    return function(input, start) {
        start = parseInt(start, 10);
        return input.slice(start);
    };
});

App.filter('orderObjectBy', function(){
    return function(input, attribute) {
        if (!angular.isObject(input)) return input;

        var array = [];
        for(var objectKey in input) {
            array.push(input[objectKey]);
        }

        array.sort(function(a, b){
            a = parseInt(a[attribute]);
            b = parseInt(b[attribute]);
            return a - b;
        });
        return array;
    }
});

App.directive('string', function() {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (value) {
                return '' + value;
            });
            ngModel.$formatters.push(function (value) {
                return parseFloat(value, 10);
            });
        }
    };
});

App.directive('ckEditor', function () {
    return {
        require: '?ngModel',
        link: function(scope, elm, attr, ngModel) {
            var ck = CKEDITOR.replace(elm[0]);

            if (!ngModel) return;

            ck.on('pasteState', function() {
                scope.$apply(function() {
                    ngModel.$setViewValue(ck.getData());
                });
            });

            ngModel.$render = function(value) {
                ck.setData(ngModel.$viewValue);
            };
        }
    };
});

function handleForm($form, onBefore, onProgress, onSuccess, onComplete) {
    $form.ajaxForm({
        dataType: 'json',
        beforeSubmit: function (formData, jqForm, options) {
            onBefore(formData, jqForm, options);
        },
        uploadProgress: function (event, position, total, percentComplete) {
            onProgress(event, position, total, percentComplete);
        },
        success: function (responseText, statusText, xhr, jqForm) {
            onSuccess(responseText, statusText, xhr, jqForm);
        },
        complete: function (xhr) {
            onComplete(xhr);
        }
    });
};

App.directive('file', function () {
    return {
        scope: {
            file: '='
        },
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var file = event.target.files[0];
                scope.file = file ? file : undefined;
                scope.$apply();
            });
        }
    };
});

App.controller('UsersController', function($scope, $http){

    $scope.editable_regime = [];

    $scope.getRegimes = function(){
        var req = $http({
            method: 'get',
            url: '/admin/regime/all/show'
        });

        req.then(function(response){

            $scope.regimes = response.data;
        });
    };

    $scope.changeRegime = function(user_id){
        $scope.regime = angular.fromJson($scope.regime);
        var data = {
            user_id: user_id,
            regime_id: $scope.regime.id
        };

        var req = $http({
            method: 'post',
            url: '/admin/regime/changeUserRegime',
            data: data
        });

        req.then(function(){

            $scope.editable_regime[user_id] = false;
            $('.regime_'+user_id).text($scope.regime.name);
        });
    };

    $scope.getRegimes();
});

App.controller('MainController', function($rootScope, $scope, $http, $window){
    $rootScope.menu = 'index';
    $rootScope._token = $window._token;
    $scope.getLanguages = function(){
        $scope.languages = ['az'];
    };
	
	$scope.changePassword = function(){
		$scope.modalPassword = $('#changePassword');
		$scope.modalPassword.modal('show');
	};
	
	$scope.updatePassword = function(id){
		$scope.password.user_id = id;
		var req = $http({
            url: '/api/password',
            method: 'post',
			data: $scope.password
        });

        req.then(function(response){
            if(response.data.success){
               $scope.modalPassword.modal('hide');
            }
        }); 
	};

    $scope.getLanguages();
});

App.controller('MenuController', function($rootScope, $scope, $http){});

App.controller('RegimeController', function($rootScope, $scope, $http){

    $scope.selected_record = [];
    $scope.selected_record_id = 0;
    $scope.action = '/admin/regime/0/edit';

    $scope.changeAction = function($event) {
        $scope.action = '/admin/regime/'+$scope.selected_record_id+'/edit';

    };

    $scope.edit = function(id, name){

        $scope.action = '/admin/regime/'+$scope.selected_record_id+'/edit';

        for(var i in $scope.selected_record){
            $('input[name="selected_active['+$scope.selected_record[i].weekday+']"]').bootstrapToggle('off');
        }

        var req = $http({
            url: '/admin/regime/details/'+id,
            method: 'get'
        });

        req.then(function(response){

            $scope.selected_record_name = name;
            $scope.selected_record_id = id;
            $scope.selected_record = [];

            for(var i in response.data){
                $scope.selected_record[response.data[i].weekday] = [];
                $scope.selected_record[response.data[i].weekday] = response.data[i];
                $scope.selected_record[response.data[i].weekday].active = response.data[i].active == 1;
                if($scope.selected_record[response.data[i].weekday].active){
                    $('input[name="selected_active['+response.data[i].weekday+']"]').bootstrapToggle('on');
                }else{
                    $('input[name="selected_active['+response.data[i].weekday+']"]').bootstrapToggle('off');
                }
            }

            $('#edit').modal('show');

        });
    };

    $scope.saveChanges = function(){
        var req = $http({
            url: '/admin/regime/'+$scope.selected_record_id+'/edit',
            method: 'post',
            data: {
                details: $scope.selected_record,
                name: $scope.selected_record_name
            }
        });

        req.then(function(response){
            if(response.data.success){
                alert('Yadda saxlanıldı');
                $('#edit').modal('hide');
            }
        });
    };

    $('.editable_checkbox').change(function () {
        var self = $(this);
        var $key = self.attr('key');
        if($scope.selected_record[$key] == null) $scope.selected_record[$key] = {};
        $scope.selected_record
        $scope.selected_record[$key].active = self.prop('checked');
        $scope.selected_record[$key].weekday = $key;

    });
});
