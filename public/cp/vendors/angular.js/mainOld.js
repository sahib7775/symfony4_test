var App = angular.module('App', ['ngRoute'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

App.config(function($routeProvider) {

    $routeProvider
        .when('/news', {
            templateUrl: '/resources/angular/admin/news.html',
            controller: 'NewsController'
        })
        .when('/blog', {
            templateUrl: '/resources/angular/admin/blog.html',
            controller: 'BlogController'
        })
        .when('/gallery', {
            templateUrl: '/resources/angular/admin/gallery.html',
            controller: 'GalleryController'
        })
        .when('/newsCategories', {
            templateUrl: '/resources/angular/admin/news_categories.html',
            controller: 'NewsCategoriesController'
        })
        .when('/blogCategories', {
            templateUrl: '/resources/angular/admin/blog_categories.html',
            controller: 'BlogCategoriesController'
        })
        .when('/galleryCategories', {
            templateUrl: '/resources/angular/admin/gallery_categories.html',
            controller: 'GalleryCategoriesController'
        })
        .when('/newsAuthors', {
            templateUrl: '/resources/angular/admin/news_authors.html',
            controller: 'NewsAuthorsController'
        })
        .when('/newsSource', {
            templateUrl: '/resources/angular/admin/news_source.html',
            controller: 'NewsSourceController'
        })
        .when('/vacancy', {
            templateUrl: '/resources/angular/admin/vacancy.html',
            controller: 'VacancyController'
        })
        .when('/vacancyCategories', {
            templateUrl: '/resources/angular/admin/vacancy_categories.html',
            controller: 'VacancyCategoriesController'
        })
        .when('/education', {
            templateUrl: '/resources/angular/admin/education.html',
            controller: 'EducationController'
        })
        .when('/experience', {
            templateUrl: '/resources/angular/admin/experience.html',
            controller: 'ExperienceController'
        })
        .when('/catalog', {
            templateUrl: '/resources/angular/admin/catalog.html',
            controller: 'CatalogController'
        })
        .when('/city', {
            templateUrl: '/resources/angular/admin/city.html',
            controller: 'CityController'
        })
        .when('/catalogSectors', {
            templateUrl: '/resources/angular/admin/catalog_sector.html',
            controller: 'NewsCategoriesController'
        })
        .when('/faq', {
            templateUrl: '/resources/angular/admin/faq.html',
            controller: 'FaqController'
        })
        .when('/workFlow', {
            templateUrl: '/resources/angular/admin/work_flow.html',
            controller: 'WorkFlowController'
        })
        .when('/rateExchange', {
            templateUrl: '/resources/angular/admin/rate_exchange.html',
            controller: 'RateExchangeController'
        }
    );
});

App.filter('offset', function() {
    return function(input, start) {
        start = parseInt(start, 10);
        return input.slice(start);
    };
});

App.filter('orderObjectBy', function(){
    return function(input, attribute) {
        if (!angular.isObject(input)) return input;

        var array = [];
        for(var objectKey in input) {
            array.push(input[objectKey]);
        }

        array.sort(function(a, b){
            a = parseInt(a[attribute]);
            b = parseInt(b[attribute]);
            return a - b;
        });
        return array;
    }
});

App.directive('string', function() {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (value) {
                return '' + value;
            });
            ngModel.$formatters.push(function (value) {
                return parseFloat(value, 10);
            });
        }
    };
});

App.directive('ckEditor', function () {
    return {
        require: '?ngModel',
        link: function(scope, elm, attr, ngModel) {
            var ck = CKEDITOR.replace(elm[0]);

            if (!ngModel) return;

            ck.on('pasteState', function() {
                scope.$apply(function() {
                    ngModel.$setViewValue(ck.getData());
                });
            });

            ngModel.$render = function(value) {
                ck.setData(ngModel.$viewValue);
            };
        }
    };
});

function handleForm($form, onBefore, onProgress, onSuccess, onComplete) {
    $form.ajaxForm({
        dataType: 'json',
        beforeSubmit: function (formData, jqForm, options) {
            onBefore(formData, jqForm, options);
        },
        uploadProgress: function (event, position, total, percentComplete) {
            onProgress(event, position, total, percentComplete);
        },
        success: function (responseText, statusText, xhr, jqForm) {
            onSuccess(responseText, statusText, xhr, jqForm);
        },
        complete: function (xhr) {
            onComplete(xhr);
        }
    });
};

App.directive('file', function () {
    return {
        scope: {
            file: '='
        },
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var file = event.target.files[0];
                scope.file = file ? file : undefined;
                scope.$apply();
            });
        }
    };
});

App.controller('LoginController', function($scope, $http){
    $scope.check = function(){
        var data = {
            username: $scope.username,
            password: $scope.password
        };

        console.log(data);
        var req = $http({
            method: 'post',
            url: '/error404/auth',
            data: data
        });

        req.then(function(response){
            if(response.data.success){
                window.location.href = "/error404";
            }
        });
    };
});

App.controller('MainController', function($rootScope, $scope, $http, $window){
    $rootScope.menu = 'index';
    $rootScope._token = $window._token;
    $scope.getLanguages = function(){
        $scope.languages = ['az'];
    };
	
	$scope.changePassword = function(){
		$scope.modalPassword = $('#changePassword');
		$scope.modalPassword.modal('show');
	};
	
	$scope.updatePassword = function(id){
		$scope.password.user_id = id;
		var req = $http({
            url: '/api/password',
            method: 'post',
			data: $scope.password
        });

        req.then(function(response){
            if(response.data.success){
               $scope.modalPassword.modal('hide');
            }
        }); 
	};

    $scope.getLanguages();
});

App.controller('MenuController', function($rootScope, $scope, $timeout){


});

App.controller('UsersController', function($rootScope, $scope, $http){
    $rootScope.$on('Menu:Users', function(event, status){
        $scope.status = status;
        $scope.all();
    });

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: '/api/users/'+$scope.status
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response.data.data;
            }
        });
    };

    $scope.create = function(){
        $scope.new_user.status = 0;
        var req = $http({
            method: 'post',
            url: '/api/users',
            data: $scope.new_user
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
                $scope.add_area = false;
                $scope.new_user = {};

            }
        });
    };
    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: '/api/users/'+ record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };
});

App.controller('NewsController', function($rootScope, $scope, $http){

    $scope.news_categories_api = '/fad2017/api/newsCategories/';
    $scope.news_authors_api = '/fad2017/api/newsAuthors/';
    $scope.news_source_api = '/fad2017/api/newsSource/';
    $scope.api = '/fad2017/api/news/';
    $scope.progress = false;

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){

                $scope.records = response.data.data.data;
            }
        });
    };

    $scope.getNewsCategories = function(){

        var req = $http({
            method: 'get',
            url: $scope.news_categories_api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.categories = response.data.data;
            }
        });
    };

    $scope.getNewsAuthors = function(){

        var req = $http({
            method: 'get',
            url: $scope.news_authors_api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.authors = response.data.data;
            }
        });
    };

    $scope.getNewsSource = function(){

        var req = $http({
            method: 'get',
            url: $scope.news_source_api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.sources = response.data.data;
            }
        });
    };

    $scope.create = function(){
        $scope.new_user.status = 0;
        var req = $http({
            method: 'post',
            url: '/api/users',
            data: $scope.new_user
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
                $scope.add_area = false;
                $scope.new_user = {};

            }
        });
    };

    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: '/api/users/'+ record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    var ck_new = true;
    $scope.modal = function(modal){
        if(ck_new){
            for(var i in $scope.$parent.languages){
                CKEDITOR.replace('text_'+$scope.$parent.languages[i]);
            }
        }
        $(modal).modal('show');
        ck_new = false;
    };

    //var $form = $('#add_news');

    /*handleForm(
        $form,
        function(formData, jqForm, options){

        },
        function(event, position, total, percentComplete){
            $scope.progress = true;
            $scope.complete = percentComplete;
        },
        function(responseText, statusText, xhr, jqForm){
            xhr.success(function(response){
                document.getElementById("add_news").reset();
                $('#news_add_area').modal('hide');
                $scope.all();
            });
        },
        function(xhr){
            $scope.progress = false;
            $scope.complete = 0;
        }
    );*/

    var ck_edit = true;

    $scope.edit = function(record){

        $scope.getSelectedRecord(record.id);
        $scope.edit_modal = $('#news_edit_area');
        $scope.edit_modal.modal('show');
    };

    $scope.getSelectedRecord = function(id){
        var req = $http({
            method: 'get',
            url: $scope.api+id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.selected_record = response.data.data;
                $scope.selected_record.category_id = $scope.selected_record.category_id.toString();
                $scope.selected_record.source_id = $scope.selected_record.source_id.toString();
            }
        });
    };

    /*var $edit_form = $('#edit_news');

    handleForm(
        $edit_form,
        function(formData, jqForm, options){

        },
        function(event, position, total, percentComplete){
            $scope.progress = true;
            $scope.complete = percentComplete;
        },
        function(responseText, statusText, xhr, jqForm){
            xhr.success(function(response){
                $scope.edit_modal.modal('hide');
                $scope.all();
            });
        },
        function(xhr){
            $scope.progress = false;
            $scope.complete = 0;
        }
    );*/

    $scope.all();
    $scope.getNewsCategories();
    $scope.getNewsAuthors();
    $scope.getNewsSource();
});

App.controller('NewsCategoriesController', function($rootScope, $scope, $http){

    $scope.new_record = {};

    $scope.editable = [];

    $scope.api = '/fad2017/api/newsCategories/';

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response.data.data;
            }
        });
    };

    $scope.create = function(){
        console.log($scope.new_record);
        var req = $http({
            method: 'post',
            url: $scope.api,
            data: $scope.new_record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
                $scope.add_area = false;
                $scope.new_record = {};

            }
        });
    };

    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: $scope.api + record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.edit_record = function(record){

        var req = $http({
            method: 'put',
            url: $scope.api+record.id,
            data: record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.editable[record.id] = false;
            }
        });
    };

    $scope.all();
});

App.controller('NewsAuthorsController', function($rootScope, $scope, $http){

    $scope.new_record = {};

    $scope.editable = [];

    $scope.api = '/fad2017/api/newsAuthors/';

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response.data.data;
            }
        });
    };

    $scope.create = function(){
        var req = $http({
            method: 'post',
            url: $scope.api,
            data: $scope.new_record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
                $scope.add_area = false;
                $scope.new_record = {};

            }
        });
    };

    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: $scope.api + record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.edit_record = function(record){

        var req = $http({
            method: 'put',
            url: $scope.api+record.id,
            data: record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.editable[record.id] = false;
            }
        });
    };

    $scope.all();
});

App.controller('NewsSourceController', function($rootScope, $scope, $http){

    $scope.new_record = {};

    $scope.editable = [];

    $scope.api = '/fad2017/api/newsSource/';

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response.data.data;
            }
        });
    };

    $scope.create = function(){
        var req = $http({
            method: 'post',
            url: $scope.api,
            data: $scope.new_record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
                $scope.add_area = false;
                $scope.new_record = {};

            }
        });
    };

    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: $scope.api + record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.edit_record = function(record){

        var req = $http({
            method: 'put',
            url: $scope.api+record.id,
            data: record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.editable[record.id] = false;
            }
        });
    };

    $scope.all();

});

App.controller('CatalogController', function($rootScope, $scope, $http){

    $scope.new_record = {};

    $scope.editable = [];

    $scope.api = '/fad2017/api/catalog/';
    $scope.sector_api = '/fad2017/api/catalogSector/';

    $scope.modal = function(modal){
        $(modal).modal('show');
    };

    $scope.getSectors = function(){
        var req = $http({
            method: 'get',
            url: $scope.sector_api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.sectors = response.data.data;
            }
        });
    };

    $scope.all = function(){
        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response.data.data;
            }
        });
    };

    var $form = $('#add_catalog');

    handleForm(
        $form,
        function(formData, jqForm, options){

        },
        function(event, position, total, percentComplete){
            $scope.progress = true;
            $scope.complete = percentComplete;
        },
        function(responseText, statusText, xhr, jqForm){
            xhr.success(function(response){
                if(response.data.success){
                    document.getElementById("add_catalog").reset();
                    $('#catalog_add_area').modal('hide');
                    $scope.all();
                }
                else{
                    alert('Logo seçilməyib');
                }
            });
        },
        function(xhr){
            $scope.progress = false;
            $scope.complete = 0;
        }
    );

    $scope.edit = function(record){
        $scope.selected_record = record;
        $('#catalog_edit_area').modal('show');
    };

    var $edit_form = $('#edit_catalog');

    handleForm(
        $edit_form,
        function(formData, jqForm, options){

        },
        function(event, position, total, percentComplete){
            $scope.progress = true;
            $scope.complete = percentComplete;
        },
        function(responseText, statusText, xhr, jqForm){
            xhr.success(function(response){
                if(response.data.success){
                    $('#catalog_edit_area').modal('hide');
                    $scope.all();
                }
            });
        },
        function(xhr){
            $scope.progress = false;
            $scope.complete = 0;
        }
    );

    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: $scope.api + record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.all();
    $scope.getSectors();

});

App.controller('CatalogSectorController', function($rootScope, $scope, $http){

    $scope.new_record = {};

    $scope.editable = [];

    $scope.api = '/fad2017/api/catalogSector/';

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response.data.data;
            }
        });
    };

    $scope.create = function(){
        var req = $http({
            method: 'post',
            url: $scope.api,
            data: $scope.new_record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
                $scope.add_area = false;
                $scope.new_record = {};

            }
        });
    };

    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: $scope.api + record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.edit_record = function(record){

        var req = $http({
            method: 'put',
            url: $scope.api+record.id,
            data: record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.editable[record.id] = false;
            }
        });
    };

    $scope.all();

});

App.controller('CatalogController', function($rootScope, $scope, $http){

    $scope.new_record = {};

    $scope.editable = [];

    $scope.api = '/fad2017/api/catalog/';
    $scope.sector_api = '/fad2017/api/catalogSector/';

    $scope.modal = function(modal){
        $(modal).modal('show');
    };

    $scope.getSectors = function(){
        var req = $http({
            method: 'get',
            url: $scope.sector_api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.sectors = response.data.data;
            }
        });
    };

    $scope.all = function(){
        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response.data.data;
            }
        });
    };

    var $form = $('#add_catalog');

    handleForm(
        $form,
        function(formData, jqForm, options){

        },
        function(event, position, total, percentComplete){
            $scope.progress = true;
            $scope.complete = percentComplete;
        },
        function(responseText, statusText, xhr, jqForm){
            xhr.success(function(response){
                if(response.data.success){
                    document.getElementById("add_catalog").reset();
                    $('#catalog_add_area').modal('hide');
                    $scope.all();
                }
                else{
                    alert('Logo seçilməyib');
                }
            });
        },
        function(xhr){
            $scope.progress = false;
            $scope.complete = 0;
        }
    );

    $scope.edit = function(record){
        $scope.selected_record = record;
        $('#catalog_edit_area').modal('show');
    };

    var $edit_form = $('#edit_catalog');

    handleForm(
        $edit_form,
        function(formData, jqForm, options){

        },
        function(event, position, total, percentComplete){
            $scope.progress = true;
            $scope.complete = percentComplete;
        },
        function(responseText, statusText, xhr, jqForm){
            xhr.success(function(response){
                if(response.data.success){
                    $('#catalog_edit_area').modal('hide');
                    $scope.all();
                }
            });
        },
        function(xhr){
            $scope.progress = false;
            $scope.complete = 0;
        }
    );

    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: $scope.api + record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.all();
    $scope.getSectors();
});

App.controller('BlogController', function($rootScope, $scope, $http){

    $scope.news_categories_api = '/fad2017/api/blogCategories/';
    $scope.news_authors_api = '/fad2017/api/newsAuthors/';
    $scope.news_source_api = '/fad2017/api/newsSource/';
    $scope.api = '/fad2017/api/blog/';
    $scope.progress = false;

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response.data.data;
            }
        });
    };

    $scope.getNewsCategories = function(){

        var req = $http({
            method: 'get',
            url: $scope.news_categories_api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.categories = response.data.data;
            }
        });
    };

    $scope.getNewsAuthors = function(){

        var req = $http({
            method: 'get',
            url: $scope.news_authors_api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.authors = response.data.data;
            }
        });
    };

    $scope.getNewsSource = function(){

        var req = $http({
            method: 'get',
            url: $scope.news_source_api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.sources = response.data.data;
            }
        });
    };

    var ck_new = true;
    $scope.modal = function(modal){
        if(ck_new){
            for(var i in $scope.$parent.languages){
                CKEDITOR.replace('text_'+$scope.$parent.languages[i]);
            }
        }
        $(modal).modal('show');
        ck_new = false;
    };

    var $form = $('#add_news');

    /*handleForm(
        $form,
        function(formData, jqForm, options){

        },
        function(event, position, total, percentComplete){
            $scope.progress = true;
            $scope.complete = percentComplete;
        },
        function(responseText, statusText, xhr, jqForm){
            xhr.success(function(response){
                document.getElementById("add_news").reset();
                $('#news_add_area').modal('hide');
                $scope.all();
            });
        },
        function(xhr){
            $scope.progress = false;
            $scope.complete = 0;
        }
    );*/

    var ck_edit = true;

    $scope.edit = function(record){

        $scope.getSelectedRecord(record.id);
        $scope.edit_modal = $('#news_edit_area');
        $scope.edit_modal.modal('show');
    };

    $scope.getSelectedRecord = function(id){
        var req = $http({
            method: 'get',
            url: $scope.api+id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.selected_record = response.data.data;
                $scope.selected_record.category_id = $scope.selected_record.category_id.toString();
                $scope.selected_record.source_id = $scope.selected_record.source_id.toString();
            }
        });
    };

    var $edit_form = $('#edit_news');

    handleForm(
        $edit_form,
        function(formData, jqForm, options){

        },
        function(event, position, total, percentComplete){
            $scope.progress = true;
            $scope.complete = percentComplete;
        },
        function(responseText, statusText, xhr, jqForm){
            xhr.success(function(response){
                $scope.edit_modal.modal('hide');
                $scope.all();
            });
        },
        function(xhr){
            $scope.progress = false;
            $scope.complete = 0;
        }
    );

    $scope.all();
    $scope.getNewsCategories();
    $scope.getNewsAuthors();
    $scope.getNewsSource();
});

App.controller('BlogCategoriesController', function($rootScope, $scope, $http){

    $scope.new_record = {};

    $scope.editable = [];

    $scope.api = '/fad2017/api/blogCategories/';

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response.data.data;
            }
        });
    };

    $scope.create = function(){
        console.log($scope.new_record);
        var req = $http({
            method: 'post',
            url: $scope.api,
            data: $scope.new_record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
                $scope.add_area = false;
                $scope.new_record = {};

            }
        });
    };

    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: $scope.api + record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.edit_record = function(record){

        var req = $http({
            method: 'put',
            url: $scope.api+record.id,
            data: record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.editable[record.id] = false;
            }
        });
    };

    $scope.all();
});

App.controller('VacancyController', function($rootScope, $scope, $http){

    $scope.categories_api = '/fad2017/api/vacancyCategories/';
    $scope.catalog_api = '/fad2017/api/catalog/';
    $scope.education_api = '/fad2017/api/education/';
    $scope.experience_api = '/fad2017/api/experience/';
    $scope.api = '/fad2017/api/vacancy/';
    $scope.progress = false;

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response.data.data;
            }
        });
    };

    $scope.getCategories = function(){

        var req = $http({
            method: 'get',
            url: $scope.categories_api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.categories = response.data.data;
            }
        });
    };

    $scope.getCatalogs = function(){

        var req = $http({
            method: 'get',
            url: $scope.catalog_api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.catalogs = response.data.data;
            }
        });
    };

    $scope.getEducation = function(){

        var req = $http({
            method: 'get',
            url: $scope.education_api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.education = response.data.data;
            }
        });
    };

    $scope.getExperience = function(){

        var req = $http({
            method: 'get',
            url: $scope.experience_api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.experience = response.data.data;
            }
        });
    };

    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: '/api/users/'+ record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    var ck_new = true;
    $scope.modal = function(modal){
        if(ck_new){
            for(var i in $scope.$parent.languages){
                CKEDITOR.replace('text_'+$scope.$parent.languages[i]);
            }
        }
        $(modal).modal('show');
        ck_new = false;
    };

    var $form = $('#add_vacancy');

    handleForm(
        $form,
        function(formData, jqForm, options){

        },
        function(event, position, total, percentComplete){
            $scope.progress = true;
            $scope.complete = percentComplete;
        },
        function(responseText, statusText, xhr, jqForm){
            xhr.success(function(response){
                document.getElementById("add_news").reset();
                $('#vacancy_add_area').modal('hide');
                $scope.all();
            });
        },
        function(xhr){
            $scope.progress = false;
            $scope.complete = 0;
        }
    );

    var ck_edit = true;

    $scope.edit = function(record){

        $scope.getSelectedRecord(record.id);
        $scope.edit_modal = $('#vacancy_edit_area');
        $scope.edit_modal.modal('show');
    };

    $scope.getSelectedRecord = function(id){
        var req = $http({
            method: 'get',
            url: $scope.api+id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.selected_record = response.data.data;
            }
        });
    };

    var $edit_form = $('#edit_vacancy');

    handleForm(
        $edit_form,
        function(formData, jqForm, options){

        },
        function(event, position, total, percentComplete){
            $scope.progress = true;
            $scope.complete = percentComplete;
        },
        function(responseText, statusText, xhr, jqForm){
            xhr.success(function(response){
                $scope.edit_modal.modal('hide');
                $scope.all();
            });
        },
        function(xhr){
            $scope.progress = false;
            $scope.complete = 0;
        }
    );

    $scope.all();
    $scope.getCategories();
    $scope.getEducation();
    $scope.getExperience();
});

App.controller('VacancyCategoriesController', function($rootScope, $scope, $http){

    $scope.new_record = {};

    $scope.editable = [];

    $scope.api = '/fad2017/api/vacancyCategories/';

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response.data.data;
            }
        });
    };

    $scope.create = function(){
        var req = $http({
            method: 'post',
            url: $scope.api,
            data: $scope.new_record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
                $scope.add_area = false;
                $scope.new_record = {};

            }
        });
    };

    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: $scope.api + record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.edit_record = function(record){

        var req = $http({
            method: 'put',
            url: $scope.api+record.id,
            data: record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.editable[record.id] = false;
            }
        });
    };

    $scope.all();
});

App.controller('GalleryController', function($rootScope, $scope, $http){

    $scope.news_categories_api = '/fad2017/api/galleryCategories/';
    $scope.news_authors_api = '/fad2017/api/newsAuthors/';
    $scope.news_source_api = '/fad2017/api/newsSource/';
    $scope.api = '/fad2017/api/gallery/';
    $scope.progress = false;

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response.data.data;
            }
        });
    };

    $scope.getNewsCategories = function(){

        var req = $http({
            method: 'get',
            url: $scope.news_categories_api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.categories = response.data.data;
            }
        });
    };

    $scope.getNewsAuthors = function(){

        var req = $http({
            method: 'get',
            url: $scope.news_authors_api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.authors = response.data.data;
            }
        });
    };

    $scope.getNewsSource = function(){

        var req = $http({
            method: 'get',
            url: $scope.news_source_api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.sources = response.data.data;
            }
        });
    };

    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: '/api/gallery/'+ record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    var ck_new = true;
    $scope.modal = function(modal){
        if(ck_new){
            for(var i in $scope.$parent.languages){
                CKEDITOR.replace('text_'+$scope.$parent.languages[i]);
            }
        }
        $(modal).modal('show');
        ck_new = false;
    };

    var $form = $('#add_gallery');

    handleForm(
        $form,
        function(formData, jqForm, options){

        },
        function(event, position, total, percentComplete){
            $scope.progress = true;
            $scope.complete = percentComplete;
        },
        function(responseText, statusText, xhr, jqForm){
            xhr.success(function(response){
                document.getElementById("add_gallery").reset();
                $('#vacancy_add_area').modal('hide');
                $scope.all();
            });
        },
        function(xhr){
            $scope.progress = false;
            $scope.complete = 0;
        }
    );

    var ck_edit = true;

    $scope.edit = function(record){

        $scope.getSelectedRecord(record.id);
        $scope.edit_modal = $('#gallery_edit_area');
        $scope.edit_modal.modal('show');
    };

    $scope.getSelectedRecord = function(id){
        var req = $http({
            method: 'get',
            url: $scope.api+id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.selected_record = response.data.data;
            }
        });
    };

    var $edit_form = $('#edit_gallery');

    handleForm(
        $edit_form,
        function(formData, jqForm, options){

        },
        function(event, position, total, percentComplete){
            $scope.progress = true;
            $scope.complete = percentComplete;
        },
        function(responseText, statusText, xhr, jqForm){
            xhr.success(function(response){
                $scope.edit_modal.modal('hide');
                $scope.all();
            });
        },
        function(xhr){
            $scope.progress = false;
            $scope.complete = 0;
        }
    );

    $scope.all();
    $scope.getNewsCategories();
    $scope.getNewsAuthors();
    $scope.getNewsSource();
});

App.controller('GalleryCategoriesController', function($rootScope, $scope, $http){

    $scope.new_record = {};

    $scope.editable = [];

    $scope.api = '/fad2017/api/galleryCategories/';

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response.data.data;
            }
        });
    };

    $scope.create = function(){
        var req = $http({
            method: 'post',
            url: $scope.api,
            data: $scope.new_record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
                $scope.add_area = false;
                $scope.new_record = {};

            }
        });
    };

    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: $scope.api + record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.edit_record = function(record){

        var req = $http({
            method: 'put',
            url: $scope.api+record.id,
            data: record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.editable[record.id] = false;
            }
        });
    };

    $scope.all();
});

App.controller('CityController', function($rootScope, $scope, $http){

    $scope.new_record = {};

    $scope.editable = [];

    $scope.api = '/fad2017/api/city/';

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response;
            }
        });
    };

    $scope.create = function(){
        var req = $http({
            method: 'post',
            url: $scope.api,
            data: $scope.new_record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
                $scope.add_area = false;
                $scope.new_record = {};

            }
        });
    };

    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: $scope.api + record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.edit_record = function(record){

        var req = $http({
            method: 'put',
            url: $scope.api+record.id,
            data: record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.editable[record.id] = false;
            }
        });
    };

    $scope.all();
});

App.controller('EducationController', function($rootScope, $scope, $http){

    $scope.new_record = {};

    $scope.editable = [];

    $scope.api = '/fad2017/api/education/';

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response;
            }
        });
    };

    $scope.create = function(){
        var req = $http({
            method: 'post',
            url: $scope.api,
            data: $scope.new_record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
                $scope.add_area = false;
                $scope.new_record = {};

            }
        });
    };

    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: $scope.api + record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.edit_record = function(record){

        var req = $http({
            method: 'put',
            url: $scope.api+record.id,
            data: record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.editable[record.id] = false;
            }
        });
    };

    $scope.all();
});

App.controller('ExperienceController', function($rootScope, $scope, $http){

    $scope.new_record = {};

    $scope.editable = [];

    $scope.api = '/fad2017/api/experience/';

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response;
            }
        });
    };

    $scope.create = function(){
        var req = $http({
            method: 'post',
            url: $scope.api,
            data: $scope.new_record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
                $scope.add_area = false;
                $scope.new_record = {};

            }
        });
    };

    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: $scope.api + record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.edit_record = function(record){

        var req = $http({
            method: 'put',
            url: $scope.api+record.id,
            data: record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.editable[record.id] = false;
            }
        });
    };

    $scope.all();
});

App.controller('RateExchangeController', function($rootScope, $scope, $http){


    $scope.editable_record = {
        id: 0
    };

    $scope.api = '/fad2017/api/rateExchange/';

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response.data.data;
            }
        });
    };

    $scope.getByDate = function(){

        var date = new Date($scope.rate_date);
        date.setMonth(date.getMonth()+1);
        var rate_date = date.getFullYear()+"-"+date.getMonth()+"-"+date.getDate();
        var req = $http({
            method: 'get',
            url: $scope.api +"?date=" + rate_date
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response.data.data;
            }
        });
    };

    $scope.setEditable = function(record){

        $scope.editable_record = record;
    };

    $scope.all();
});

App.controller('FaqController', function($rootScope, $scope, $http){

    $scope.new_record = {};

    $scope.editable = [];

    $scope.api = '/fad2017/api/faq/';

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.records = response;
            }
        });
    };

    $scope.create = function(){
        var req = $http({
            method: 'post',
            url: $scope.api,
            data: $scope.new_record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
                $scope.add_area = false;
                $scope.new_record = {};

            }
        });
    };

    $scope.delete = function(record){
        var req = $http({
            method: 'delete',
            url: $scope.api + record.id
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.edit_record = function(record){

        var req = $http({
            method: 'put',
            url: $scope.api+record.id,
            data: record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.editable[record.id] = false;
            }
        });
    };

    $scope.all();
});

App.controller('WorkFlowController', function($rootScope, $scope, $http){

    $scope.new_record = {};

    $scope.records_array = [];
    $scope.admins_array = [];

    $scope.editable = [];

    $scope.api = '/fad2017/workFlow/';

    $scope.getAdmins = function(){

        var req = $http({
            method: 'get',
            url: '/fad2017/api/admins'
        });

        req.then(function(response){
            if(response.data.success){
                $scope.admins = response.data;
                for(var i in $scope.admins){
                    $scope.admins_array[$scope.admins[i].id] = $scope.admins[i];
                }
            }
        });
    };

    $scope.all = function(){

        var req = $http({
            method: 'get',
            url: $scope.api
        });

        req.then(function(response){
            if(response.data.success){
                $scope.firstStep = [];
                $scope.secondStep = [];
                $scope.thirdStep = [];
                $scope.fourthStep = [];
                $scope.fifthStep = [];
                $scope.deleted = [];
                $scope.records = response.data.data;
                for(var i in $scope.records){
                    switch ($scope.records[i].status){
                        case 0:
                            $scope.deleted.push($scope.records[i]);
                            break;
                        case 1:
                            $scope.firstStep.push($scope.records[i]);
                            break;
                        case 2:
                            $scope.secondStep.push($scope.records[i]);
                            break;
                        case 3:
                            $scope.thirdStep.push($scope.records[i]);
                            break;
                        case 4:
                            $scope.fourthStep.push($scope.records[i]);
                            break;
                        case 5:
                            $scope.fifthStep.push($scope.records[i]);
                            break;
                        default:
                            $scope.fourthStep.push($scope.records[i]);
                            break;
                    }

                    $scope.records_array[$scope.records[i].id] = $scope.records[i];
                }
            }
        });
    };

    $scope.openText = function(record){
        $scope.selected_record = record;
        $('#edit_text').modal('show');
    };

    $scope.create = function(){
        var req = $http({
            method: 'post',
            url: $scope.api,
            data: $scope.new_record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
                $scope.add_area = false;
                $scope.new_record = {};

            }
        });
    };

    $scope.editByAdministration = function(record){
        var req = $http({
            method: 'post',
            url: $scope.api + 'editByAdministration/' + record.id,
            data: record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.editByWriter = function(record){

        var req = $http({
            method: 'post',
            url: $scope.api + 'editByWriter/'+record.id,
            data: record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.editByEditor = function(record){
        var req = $http({
            method: 'post',
            url: $scope.api + 'editByEditor/' + record.id,
            data: record
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.setStatus = function(record, status){
        var req = $http({
            method: 'post',
            url: $scope.api + 'setStatus/' + record.id + "/" + status
        });

        req.then(function(response){
            if(response.data.success){
                $scope.all();
            }
        });
    };

    $scope.addComment = function(){
        var req = $http({
            method: 'post',
            url: '/fad2017/workFlow/addComment',
            data: {
                id: $scope.comment_record,
                comment: $scope.new_comment
            }
        });

        req.then(function(response){
            if(response.data.success){
                $scope.new_comment = '';
                return $scope.getComments($scope.comment_record);
            }

        },function(){});
    };

    $scope.getComments = function(id){
        var req = $http({
            method: 'get',
            url: '/fad2017/workFlow/getComments/'+id
        });

        req.then(function(response){
            if(response.data.success) {
                if (response.data.success) {
                    $scope.comments = response.data.data
                }
            }

        },function(){});
    };

    $scope.openComments = function(id){
        $scope.getComments(id);
        $scope.comment_record = id;
        $('#comments').modal('show');
    };

    $scope.all();
    $scope.getAdmins();
});